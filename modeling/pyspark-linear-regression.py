from pyspark.ml.regression import LinearRegression
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
sc = SparkContext.getOrCreate();
spark = SparkSession(sc)


# FOR LA-COUNTY
#fileIn = "../data/la-county/la-county-prepared.libsvm"

# FOR ORANGE-COUNTY
#fileIn = "../data/orange-county/orange-county-prepared.libsvm"

# FOR SD-COUNTY
#fileIn = "../data/sd-county/sd-county-prepared.libsvm"


# Load training data
training = spark.read.format("libsvm")\
    .load(fileIn)

lr = LinearRegression(maxIter=10, regParam=0.3, elasticNetParam=0.8)

# Fit the model
lrModel = lr.fit(training)

# Print the coefficients and intercept for linear regression
# print("Coefficients: %s" % str(lrModel.coefficients))
# print("Intercept: %s" % str(lrModel.intercept))

# Summarize the model over the training set and print out some metrics
trainingSummary = lrModel.summary
print("numIterations: %d" % trainingSummary.totalIterations)
print("objectiveHistory: %s" % str(trainingSummary.objectiveHistory))
trainingSummary.residuals.show()
print("RMSE: %f" % trainingSummary.rootMeanSquaredError)
print("r2: %f" % trainingSummary.r2)
