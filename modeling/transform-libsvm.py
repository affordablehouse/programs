import pandas as pd
import sklearn.datasets as sd

# data columns
# 0 PROPERTY TYPE
# 1 ZIP CODE
# 2 PRICE
# 3 BEDS
# 4 BATHS
# 5 LOCATION
# 6 SQUARE FEET
# 7 LOT SIZE
# 8 YEAR BUILT
# 9 HOA/MONTH


# FOR LA-COUNTY
#fileIn = "../data/la-county/la-county-clean-no-anomalies.csv"
#fileOut = "../data/la-county/la-county-prepared.libsvm"
#fileOutNormal = "../data/la-county/la-county-normalized.libsvm"

# FOR ORANGE-COUNTY
#fileIn = "../data/orange-county/orange-county-clean-no-anomalies.csv"
#fileOut = "../data/orange-county/orange-county-prepared.libsvm"
#fileOutNormal = "../data/orange-county/orange-county-normalized.libsvm"

# FOR SD-COUNTY
#fileIn = "../data/sd-county/sd-county-clean-no-anomalies.csv"
#fileOut = "../data/sd-county/sd-county-prepared.libsvm"
#fileOutNormal = "../data/sd-county/sd-county-normalized.libsvm"



# load dataset into dataframe with pandas
df = pd.read_csv(fileIn, header=None)
print(df)

# drop rows with missing values
df = df.dropna()

class_labels = df[2]
print(class_labels)

# create a new empty dataframe and add classes to first column
new_df = pd.DataFrame()
new_df[0] = class_labels

# add columns we aren't transforming to new dataframe
new_df[1] = df[3]
new_df[2] = df[4]
new_df[3] = df[6]
new_df[4] = df[7]
new_df[5] = df[8]
new_df[6] = df[9]
print(new_df)

targets = [0,1,5]
# counter for column adding next
col = 7

# loop through columns
for target in targets:
    # get_dummies converts categorical data into indicator variables
    x = pd.get_dummies(df[target])
    
    # loop through columns in x and add them to new dataframe
    for j in range(0, len(x.columns)):
        new_df[col] = x[x.columns[j]]
        col += 1
            
print(new_df)


# normalize data
normalized_df=(new_df-new_df.min())/(new_df.max()-new_df.min())
#print(normalized_df)

norm_labels = normalized_df[0]
norm_train_data = normalized_df.iloc[:, 1:len(normalized_df.columns)]

class_labels = new_df[0]
train_data = new_df.iloc[:, 1:len(new_df.columns)]

sd.dump_svmlight_file(train_data, class_labels, fileOut, zero_based=False)
sd.dump_svmlight_file(norm_train_data, norm_labels, fileOutNormal, zero_based=False)
