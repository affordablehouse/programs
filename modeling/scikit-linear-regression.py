import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from statsmodels.graphics.gofplots import qqplot_2samples

# data columns
# 0 PROPERTY TYPE
# 1 ZIP CODE
# 2 PRICE
# 3 BEDS
# 4 BATHS
# 5 LOCATION
# 6 SQUARE FEET
# 7 LOT SIZE
# 8 YEAR BUILT
# 9 HOA/MONTH

# FOR LA-COUNTY
#fileIn = "../data/la-county/la-county-clean-no-anomalies.csv"
#fileOut = "../data/la-county/la-county-prepared.csv"


# FOR ORANGE-COUNTY
#fileIn = "../data/orange-county/orange-county-clean-no-anomalies.csv"
#fileOut = "../data/orange-county/orange-county-prepared.csv"


# FOR SD-COUNTY
#fileIn = "../data/sd-county/sd-county-clean-no-anomalies.csv"
#fileOut = "../data/sd-county/sd-county-prepared.csv"


# load dataset and make new file with a header and the right columns
df = pd.read_csv(fileIn, header=None)
print(df.head)
df = df.dropna()

# rebuild file without nominal values - property_type, zip_code, location
new_df = pd.DataFrame()
new_df[0] = df[3]
new_df[1] = df[4]
new_df[2] = df[6]
new_df[3] = df[7]
new_df[4] = df[8]
new_df[5] = df[9]
new_df[6] = df[2]


new_df.to_csv(fileOut, index=False, header=["beds", "baths", "sqft", "lot", "year_built", "hoa", "price"])


# read prepared file
data = pd.read_csv(fileOut)
print(data.head())


# Printing shape of data
print("Shape of data:", data.shape)

# Extracting Independent variables
X = data.drop(["price"],axis=1) # Drop the target variable, retain all other variables
print("Shape of independent variables:", X.shape)

# Extracting Dependent variable
y = data["price"] # Select only the target variable
print("Shape of dependent variable:", y.shape)

# A 70/30 train/test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=100, shuffle=True)

print("Shape of X_train:", X_train.shape)
print("Shape of X_test:", X_test.shape)
print("Shape of y_train:", y_train.shape)
print("Shape of y_test:", y_test.shape)


# Fitting a linear model
model = LinearRegression()
# Training our model
model.fit(X_train,y_train)

# Printing intercept
print("Intercept:",model.intercept_)

# Printing coefficients for the given features using a pandas dataframe
model_coefs = pd.DataFrame({'Feature/Column': list(X.columns), 'Coefficient': model.coef_})
# coefficients close to 0 add less value to model
print(model_coefs)

# Taking predictions for the test set
y_pred = model.predict(X_test)


# Comparing shapes of predicted and real values - same shape
print("Shape of y_test:", y_test.shape)
print("Shape of y_pred:", y_pred.shape)


# model evaluation
# Importing metrics from Scikit-learn
from sklearn.metrics import r2_score, mean_squared_error
# Calculating metrics
print("The R-squared score is {:.4f}".format(r2_score(y_test,y_pred)))
print("The Root Mean Squared error is {:.4f}".format(np.sqrt(mean_squared_error(y_test,y_pred))))

# Residuals plot(overall)

#plt.figure(figsize=(15,4))
#plt.scatter(y_pred,y_test-y_pred)
#plt.title("Residuals plot")
#plt.xlabel("Predicted value")
#plt.ylabel("Residuals")

# histogram of residuals

#plt.figure(figsize=(10,4))
#plt.hist(y_test-y_pred, bins= 20)
#plt.title("Histogram of residuals")

## Normal QQ-plot

qqplot_2samples(y_test,y_pred,line='45')
plt.title("QQ-plot")
plt.xlabel("Quantiles of observed values")
plt.ylabel("Quantiles of predicted values")



plt.show()