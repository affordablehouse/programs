# Pricing a House with Data #

Assessing residential property value is difficult; professionals within the real estate industry use a variety of metrics resulting in differing values and people outside the industry have few resources.

This is a tool that allows a user to enter variables about a house into a form that returns an accurate price distilled from data that have been collected, cleaned, analyzed and processed into a prediction model.

### Project Team ###

* Lilia Covarrubias
* Mary Ann Cruz
* Melissa Gore