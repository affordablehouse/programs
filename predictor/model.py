# https://www.datacamp.com/community/tutorials/machine-learning-models-api-python
import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import joblib

import pickle

# data columns
# 0 PROPERTY TYPE
# 1 ZIP CODE
# 2 PRICE
# 3 BEDS
# 4 BATHS
# 5 LOCATION
# 6 SQUARE FEET
# 7 LOT SIZE
# 8 YEAR BUILT
# 9 HOA/MONTH
# 10 County

fileIn = "data/data.csv"
fileOut = "data/data-prepared.csv"
typesOut = "data/data-types.csv"
zipsOut = "data/data-zips.csv"
locationsOut = "data/data-locations.csv"
countiesOut = "data/data-counties.csv"

# load dataset and make new file with a header and the right columns
df = pd.read_csv(fileIn, header=None)
df = df.dropna()

types_df = pd.DataFrame()
types_df[0] = df[0]

zips_df = pd.DataFrame()
zips_df[0] = df[1].map(str).str[:5]

locations_df = pd.DataFrame()
locations_df[0] = df[5]

counties_df = pd.DataFrame()
counties_df[0] = df[10]

le = preprocessing.LabelEncoder()
df[0] = le.fit_transform(df[0])
df[1] = le.fit_transform(df[1])
df[5] = le.fit_transform(df[5])
df[10] = le.fit_transform(df[10])

types_df[1] = df[0]
types_df = types_df.drop_duplicates()
types_df.to_csv(typesOut, index=False, header=False)

zips_df[1] = df[1]
zips_df = zips_df.drop_duplicates()
zips_df.to_csv(zipsOut, index=False, header=False)

locations_df[1] = df[5]
locations_df = locations_df.drop_duplicates()
locations_df.to_csv(locationsOut, index=False, header=False)

counties_df[1] = df[10]
counties_df = counties_df.drop_duplicates()
counties_df.to_csv(countiesOut, index=False, header=False)

# rebuild file without nominal values - zip_code, location
new_df = pd.DataFrame()
new_df[0] = df[0]
new_df[1] = df[1]
new_df[2] = df[3]
new_df[3] = df[4]
new_df[4] = df[5]
new_df[5] = df[6]
new_df[6] = df[7]
new_df[7] = df[8]
new_df[8] = df[9]
new_df[9] = df[10]
new_df[10] = df[2]


new_df.to_csv(fileOut, index=False, header=["type", "zip", "beds", "baths", "location",
                                            "sqft", "lot", "year", "hoa", "county", "price"])
# read prepared file
data = pd.read_csv(fileOut)
print(data.head())


# Printing shape of data
print("Shape of data:", data.shape)

# Extracting Independent variables
X = data.drop(["price"], axis=1) # Drop the target variable, retain all other variables
print("Shape of independent variables:", X.shape)

# Extracting Dependent variable
y = data["price"] # Select only the target variable
print("Shape of dependent variable:", y.shape)

# A 70/30 train/test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=100, shuffle=True)

print("Shape of X_train:", X_train.shape)
print("Shape of X_test:", X_test.shape)
print("Shape of y_train:", y_train.shape)
print("Shape of y_test:", y_test.shape)


# Fitting a linear model
model = LinearRegression()
# Training our model
model.fit(X_train, y_train)

# Printing intercept
print("Intercept:", model.intercept_)

# Printing coefficients for the given features using a pandas dataframe
model_coefs = pd.DataFrame({'Feature/Column': list(X.columns), 'Coefficient': model.coef_})
# coefficients close to 0 add less value to model
print(model_coefs)

# Taking predictions for the test set
y_pred = model.predict(X_test)


# Comparing shapes of predicted and real values - same shape
print("Shape of y_test:", y_test.shape)
print("Shape of y_pred:", y_pred.shape)


# model evaluation
# Importing metrics from Scikit-learn
from sklearn.metrics import r2_score, mean_squared_error
# Calculating metrics
print("The R-squared score is {:.4f}".format(r2_score(y_test, y_pred)))
RMSE = np.sqrt(mean_squared_error(y_test,y_pred))
print("The normalized RMSE is {:.4f}".format(RMSE/10993999))
print("The RMSE is {:.4f}".format(np.sqrt(mean_squared_error(y_test,y_pred))))

joblib.dump(model, 'model.pkl')

# Load the model that you just saved
model = joblib.load('model.pkl')

# Saving the data columns from training
model_columns = list(X.columns)
joblib.dump(model_columns, 'model_columns.pkl')
