let predictBtn = document.getElementById("get-price-btn");

function getPrediction(evt) {
    if (evt.type == "click") {
        evt.preventDefault();
    }
    let url = "http://127.0.0.1:5000/predict";

    let xhr = new XMLHttpRequest();
    xhr.open("POST", url);

    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function () {

       if (xhr.readyState === 4) {
            let response = JSON.parse(xhr.responseText);
            let prediction = parseInt(response.prediction.replace(/[\[\]']+/g,''));

            document.getElementById("show-prediction").innerHTML = '$' + prediction.toLocaleString();
       }
    };

    let baths = document.getElementById("baths").value;
    let beds = document.getElementById("beds").value;
    let county = document.getElementById("county").value;
    let hoa = document.querySelector('input[name="hoa"]:checked').value
    let location = document.getElementById("select-location").value;
    let lot = document.getElementById("lot").value;
    let sqft = document.getElementById("sqft").value;
    let type = document.getElementById("type").value;
    let year = document.getElementById("year").value;
    let zip = document.getElementById("select-zip").value;

    let data = `[{
      "baths": ` + baths + `,
      "beds": ` + beds + `,
      "county": ` + county + `,
      "hoa": ` + hoa + `,
      "location": ` + location + `,
      "lot": ` + lot + `,
      "sqft": ` + sqft + `,
      "type": ` + type + `,
      "year": ` + year + `,
      "zip": ` + zip + `
    }]`;

    console.log(data);

    xhr.send(data);

}

predictBtn.addEventListener("click", getPrediction, false);
