WLA - West Los Angeles
HPK - Hancock Park
KREA - Koreatown
Mid-Wilshire
Hancock Park-Wilshire
C17 - Mid-Wilshire
C42 - Downtown L.A.
PHHT - Park Hills Heights
C34 - Los Angeles Southwest
Park Hills Heights
Silver Lake - Echo Park
Downtown L.A.
Metropolitan
CHNA - Chinatown
Beverly Center-Miracle Mile
CO - Los Angeles (County)
C05 - Westwood - Century City
Westwood - Century City
West L.A.
Los Feliz
Hollywood
C20 - Hollywood
677 - Lincoln Hts
Lincoln Heights
699 - Not Defined
El Sereno
C09 - Beverlywood Vicinity
Beverlywood Vicinity
Palms - Mar Vista
C16 - Mid Los Angeles
C19 - Beverly Center-Miracle Mile
671 - Silver Lake
676 - Monterey Hills
Monterey Hills
632 - Highland Park
C29 - Westchester
West Hollywood Vicinity
Sunset Strip - Hollywood Hills West
West Hollywood
C10 - West Hollywood Vicinity
C03 - Sunset Strip - Hollywood Hills West
Not Applicable
Brentwood
C06 - Brentwood
Ladera Heights
C36 - Metropolitan Southwest
Glassell Park
Culver City
Playa Vista
C39 - Playa Vista
Beverly Hills
C01 - Beverly Hills
C28 - Culver City
D4 - Southeast Downey, S of Firestone, E of Downey
141 - El Segundo
118 - Pacific Square
119 - Central Gardena
North Gardena
117 - McCarthy
McCarthy
107 - Holly Glen/Del Aire
149 - Hermosa Bch Valley
T1 - Vernon, Maywood, Hunt Pk & Bell, N of Florenc
North Lawndale
RM - Lynwood
Pacific Palisades
165 - PV Dr North
169 - PV Dr South
174 - Crest
177 - Eastview/RPV
Crest
157 - S Redondo Bch W of PCH
156 - S Redondo Bch S of Torrance Bl
128 - Hollywood Riviera
N. Redondo Beach/Villas North
151 - N Redondo Bch/Villas North
152 - N Redondo Bch/Villas South
N. Redondo Beach/Villas South
Venice
Marina Del Rey
C12 - Marina Del Rey
Playa Del Rey
C31 - Playa Del Rey
101 - North Inglewood
North Inglewood
Santa Monica
C14 - Santa Monica
123 - County Strip
126 - Central Torrance
Central Torrance
127 - South East Torrance - West
135 - South East Torrance - East
125 - Walteria
South Torrance
670 - Whittier
M3 - La Mirada
674 - Montebello
Montebello
M1 - Norwalk
RC - Artesia
RA - Cerritos North of 91 Frwy
RD - Cerritos South of 91 Frwy, W of Norwalk
RG - Bellflower North of 91 Frwy, S of Alondra
124 - Harbor City
54 - Hawaiian Gardens
121 - Lomita
RK - Paramount South of Somerset
Holy Trinity
185 - Plaza
Point Fermin
181 - Point Fermin
183 - Vista Del Oro
187 - Holy Trinity
186 - Miraleste Pines
193 - San Pedro - North
195 - West Wilmington
137 - North Carson
136 - Central Carson
8 - Signal Hill
4 - Downtown Area, Alamitos Beach
Downtown Area, Alamitos Beach
2 - Belmont Heights, Alamitos Heights
1 - Belmont Shore/Park,Naples,Marina Pac,Bay Hrbr
3 - Eastside, Circle Area
36 - Park Estates
7 - North Long Beach
Long Beach
6 - Bixby, Bixby Knolls, Los Cerritos
11 - Westside
5 - Wrigley Area
Belmont Heights, Alamitos Heights
605 - Arcadia
Arcadia
617 - Duarte
639 - Monrovia
658 - So. Pasadena
South Pasadena
648 - Pasadena (SE)
647 - Pasadena (SW)
646 - Pasadena (NE)
645 - Pasadena (NW)
Pasadena SW
Pasadena SE
626 - Glendale-Northwest
Glendale
628 - Glendale-South of 134 Fwy
627 - Rossmoyne & Verdu Woodlands
Glendale-South of 134 Fwy
635 - La Crescenta/Glendale Montrose & Annex
La Crescenta/Glendale/Montrose/Sparr Heights
AGOA - Agoura
Agoura Hills
CP - Canoga Park
WIN - Winnetka
Winnetka
CHT - Chatsworth
Encino
ENC - Encino
DNEW - Downtown Newhall
NEW4 - Newhall 4
NEW1 - Newhall 1
NEW5 - Newhall Area 5
Northridge
NR - Northridge
PORA - Porter Ranch
RES - Reseda
SYL - Sylmar
North Hills
BOUQ - Bouquet Canyon
PLUM - Plum Canyon
Canyon Country 1
CAN1 - Canyon Country 1
RBGL - Rainbow Glen
Canyon Country
Sun Valley
NBRG - Valencia Northbridge
VAL1 - Valencia 1
TAR - Tarzana
WV - Westlake Village
WHLL - Woodland Hills
STEV - Stevenson Ranch
Stevenson Ranch
VWES - Valencia Westridge
CAN2 - Canyon Country 2
Canyon Country 2
CAN3 - Canyon Country 3
VN - Van Nuys
PC - Panorama City
SO - Sherman Oaks
610 - Burbank
STUD - Studio City
North Hollywood
NHO - North Hollywood
VVL - Valley Village
607 - Azusa
608 - Baldwin Pk/Irwindale
683 - Claremont
614 - Covina
619 - El Monte
El Monte
633 - Industry/La Puente/Valinda
631 - Hacienda Heights
641 - Monterey Park
616 - Diamond Bar
687 - Pomona
651 - Rosemead/S. San Gabriel
689 - San Dimas
San Dimas
654 - San Gabriel
661 - Temple City
668 - Walnut
Walnut
669 - West Covina
601 - Alhambra
Alhambra
Lancaster
LAC - Lancaster
PLM - Palmdale
Palmdale
148 - Hermosa Bch Sand
131 - West Torrance
133 - N Torrance - East
RF - Bellflower South of 91 Frwy
179 - South Shores
West Hills
MHL - Mission Hills
ACTO - Acton
ANTA - Antelope Acres
Littlerock
Llano
C37 - Metropolitan South
Metropolitan South
WATT - Watts
Los Angeles
ELA - East Los Angeles
Mid Los Angeles
C23 - Metropolitan
Los Angeles Southwest
East LA
BOYH - Boyle Heights
637 - Los Feliz
679 - Montecito Heights
Montecito Heights
621 - El Sereno
Cheviot Hills - Rancho Park
C13 - Palms - Mar Vista
Atwater
606 - Atwater
Eagle Rock
618 - Eagle Rock
Highland Park
WIND - Windsor Hills
HYDE - Hyde Park
VWPK - View Park
Metropolitan Southwest
Westchester
Compton West of Central
106 - Los Angeles
122 - Harbor Gateway
City Terrace
CTER - City Terrace
Boyle Heights
Mt Washington
680 - Mount Washington
623 - Glassel Park
CYPK - Cypress Park
Hollywood Hills East
C30 - Hollywood Hills East
Bel Air - Holmby Hills
C04 - Bel Air - Holmby Hills
T3 - Bell Gardens, Bell E of 710, Commerce S of 26
T6 - Maywood, Bell
Beverly Hills Post Office
RP - Compton S of Rosecrans, E of Central,W of Ala
Compton S of Rosecrans, E of Central,W of Alameda
RO - Compton S of Rosecrans, E of Alameda
RN - Compton N of Rosecrans, E of Central
Compton S of Rosecrans, E of Alameda
D1 - Northeast Downey, N of Firestone, E of Downey
D2 - Northwest Downey, N of Firestone, W of Downey
D3 - Southwest Downey, S of Firestone, W of Downey
El Segundo
North Gateway
116 - North Gateway
RQ - Compton West of Central
120 - South Gardena
114 - Hollypark
108 - North Hawthorne
109 - Ramona/Burleigh
North Hawthorne
110 - East Hawthorne
111 - Bodger Park/El Camino
Bodger Park/El Camino Village
Hermosa Beach East
150 - Hermosa Bch East
T5 - WalnutPk, HuntPk, Bell N of Florence, and Cud
113 - South Lawndale
112 - North Lawndale
South Lawndale
C33 - Malibu
Malibu
146 - Manhattan Bch Heights/Lib Vlg
143 - Manhattan Bch Tree
142 - Manhattan Bch Sand
144 - Manhattan Bch Hill
C15 - Pacific Palisades
160 - Lunada Bay/Margate
172 - La Cresta
166 - Rolling Hills
164 - Valmonte
162 - Monte Malaga
176 - Silver Spur
167 - PV Dr East
173 - Los Verdes
170 - West Palos Verdes
Palos Verdes Drive East
171 - Country Club
155 - S Redondo Bch N of Torrance Bl
153 - N Redondo Bch/El Nido
154 - N Redondo Bch/Golden Hills
Cudahy, South Gate W of 710, Hunt Pk S of Florence
T4 - South Gate E of 710
TOP - Topanga
Topanga
C11 - Venice
102 - South Inglewood
Inglewood
South Inglewood
105 - Lennox
134 - Old Torrance
130 - Southwood
Southwood
132 - N Torrance - West
129 - South Torrance
678 - N. Whittier
Whittier
80 - Cypress North of Katella
88 - La Habra Heights
RE - Norwalk South of Alondra
Norwalk
649 - Pico Rivera
M2 - Santa Fe Springs
Santa Fe Springs
RB - Cerritos South of 91 Frwy, E of Norwalk
RH - Bellflower N of Alondra, E of Bellflower
RJ - Bellflower N of Alondra, W of Bellflower
Harbor City
30 - Lakewood Country Club Estates
24 - Lakewood Mutuals
21 - Crest Gardens, Mayfair, Signature
23 - Lakewood Park
25 - Carson Park
29 - Lakewood Village
MAYF - Mayfair
Carson Park
22 - Lakewood Estates, Lakewood Manor
26 - Lakewood East
Lomita
RL - Paramount North of Somerset
189 - Barton Hill
1A - Seal Beach
West Wilmington
139 - South Carson
South Carson
North Carson
138 - Rancho Dominguez
140 - East Carson
Signal Hill
North Long Beach
9 - Poly High
Wrigley
Bixby, Bixby Knolls, Los Cerritos
33 - Lakewood Plaza, Rancho
42 - El Dorado Park
31 - South of Conant
28 - Lakewood City
27 - Imperial Estates North
Lakewood Plaza, Rancho
35 - Artcraft Manor
34 - Los Altos, X-100
32 - Stratford Square, University
604 - Altadena
Altadena
634 - La Canada Flintridge
Monrovia
656 - Sierra Madre
Sierra Madre
659 - Sunland/Tujunga
Shadow Hills
Pasadena
Pasadena NE
Pasadena NW
655 - San Marino
Glendale-Northwest
624 - Glendale-Chevy Chase/E. Glenoaks
Glendale-Chevy Chase/E. Glen Oaks
Rossmoyne & Verdugo Woodlands
Agoura
Calabasas
CLB - Calabasas
WEH - West Hills
Canoga Park
Chatsworth
SHFO - Sherwood Forest
Porter Ranch
Tarzana
Reseda
SF - San Fernando
673 - Lake View Terrace
NOH - North Hills
GH - Granada Hills
Granada Hills
CJRC - Circle J Ranch
Plum Canyon
VMET - Villa Metro
COPN - Copper Hill North
SKYLN - Skyline Ranch
Bouquet Canyon
Rainbow Glen
Tesoro de Valle
VALN - Valencia North
VALC - Valencia Copperhill
CRSD - Valencia Creekside
NPRK - Valencia Northpark
VLWC - Valencia West Creek
TSRO - Tesoro de Valle
Valencia 1
VALB - Valencia Bridgeport
VSUM - Valencia Summit
Woodland Hills
HASC - Hasley Canyon
VVER - Val Verde
CJCT - Castaic Junction
PRKR - Parker Road
HILC - Hillcrest Area
LOAK - Live Oaks
HSHL - Hasley Hills
Hillcrest Area
NCAS - North Castaic
ALIE - Aliento
SAND - Sand Canyon
VACN - Vista Canyon
Santa Clarita
ADUL - Agua Dulce
Agua Dulce
LEL - Lake Elizabeth
VG - Valley Glen
Sherman Oaks
East Van Nuys
Lake Balboa
LKBL - Lake Balboa
Burbank
Studio City
Valley Village
Azusa
Baldwin Park/Irwindale
681 - Chino
Covina
629 - Glendora
Glendora
652 - Rowland Heights
Rowland Heights
684 - La Verne
La Verne
Monterey Park
693 - Mt Baldy
685 - Montclair
Pomona
ROSEMEAD
San Gabriel
West Covina
Acton
Lake Hughes
Lake Elizabeth
Lake Los Angeles
LKL - Lake Los Angeles
Quartz Hill
QHL - Quartz Hill
FAIR - Fairmont
LRK - Littlerock
LLO - Llano
Leona Valley
PALMDALE
Pearblossom
JHLS - Juniper Hills
Pacific Square
Holly Glen/Del Aire
147 - Manhattan Bch Mira Costa
Manhattan Beach Tree
North Torrance East
Old Torrance
County Strip
Cerritos North of 91 Frwy
Bellflower South of 91 Frwy
Vista del Oro
Central Carson
Newhall 4
Valencia Northpark
657 - So. El Monte
