# https://www.datacamp.com/community/tutorials/machine-learning-models-api-python
import sys
from flask import Flask, request, jsonify, render_template
import joblib
import traceback
import pandas as pd
from forms import PropertyForm

# Your API definition
app = Flask(__name__)
app.config['SECRET_KEY'] = 'your secret key'


@app.route('/', methods=['GET', 'POST'])
def index():
    beds = None
    baths = None
    sqft = None
    lot = None
    year = None
    type = None
    hoa = None
    county = None
    location = None
    zip = None
    form = PropertyForm()
    if form.validate_on_submit():
        beds = form.beds.data
        form.beds.data = ''
        baths = form.baths.data
        form.baths.data = ''
        sqft = form.sqft.data
        form.sqft.data = ''
        lot = form.lot.data
        form.lot.data = ''
        year = form.year.data
        form.year.data = ''
        type = form.type.data
        form.type.data = ''
        hoa = form.hoa.data
        form.hoa.data = ''
        county = form.county.data
        form.county.data = ''
        location = form.location.data
        form.location.data = ''
        zip = form.zip.data
        form.zip.data = ''
    return render_template('index.html', form=form, beds=beds, baths=baths,
                           sqft=sqft, lot=lot, year=year, type=type, hoa=hoa,
                           county=county, location=location, zip=zip)


@app.route('/privacy-policy')
def pp():
    return render_template('privacy-policy.html')


@app.route('/predict', methods=['POST'])
def predict():
    if model:
        try:
            json_ = request.json
            print(json_)
            query = pd.get_dummies(pd.DataFrame(json_))
            query = query.reindex(columns=model_columns, fill_value=0)

            prediction = list(model.predict(query))

            return jsonify({'prediction': str(prediction)})

        except:

            return jsonify({'trace': traceback.format_exc()})
    else:
        print('Train the model first')
        return 'No model here to use'


if __name__ == '__main__':
    try:
        port = int(sys.argv[1])  # This is for a command-line input
    except:
        port = 5000  # If you don't provide any port the port will be set to 12345

    model = joblib.load("model.pkl")  # Load "model.pkl"
    model_columns = joblib.load("model_columns.pkl")  # Load "model_columns.pkl"

    app.run(port=port, debug=True)
