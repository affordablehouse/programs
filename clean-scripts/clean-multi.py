"""
Remove properties with property type of multi-family
"""

import pandas as pd

# data columns
# 0 PROPERTY TYPE
# 1 ZIP CODE
# 2 PRICE
# 3 BEDS
# 4 BATHS
# 5 LOCATION
# 6 SQUARE FEET
# 7 LOT SIZE
# 8 YEAR BUILT
# 9 HOA/MONTH

fileIn = "../data/la-county/la-county-clean.csv"
fileOut = "../data/la-county/la-county-clean.csv"

# read file to be cleaned
df = pd.read_csv(fileIn, header=None)

# filter to only keep records where property type does not start with Multi
df_filtered = df[~df[0].str.startswith('Multi')]

# output new file
df_filtered.to_csv(fileOut, header=None, index=False)
