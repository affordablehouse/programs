"""
Get unique locations
"""

import pandas as pd
import numpy as np

# data columns
# 0 PROPERTY TYPE
# 1 ZIP CODE
# 2 PRICE
# 3 BEDS
# 4 BATHS
# 5 LOCATION
# 6 SQUARE FEET
# 7 LOT SIZE
# 8 YEAR BUILT
# 9 HOA/MONTH


fileIn = "../data/la-county/la-county-clean-no-anomalies.csv"
locationsOut = "../html/autocomplete/la-county-locations.txt"
zipsOut = "../html/autocomplete/la-county-zips.txt"


#fileIn = "../data/sd-county/sd-county-clean-no-anomalies.csv"
#locationsOut = "../html/autocomplete/sd-county-locations.txt"
#zipsOut = "../html/autocomplete/sd-county-zips.txt"


#fileIn = "../data/orange-county/orange-county-clean-no-anomalies.csv"
#locationsOut = "../html/autocomplete/orange-county-locations.txt"
#zipsOut = "../html/autocomplete/orange-county-zips.txt"

# load dataset into dataframe with pandas
df = pd.read_csv(fileIn, header=None)
print(df)

# drop rows with missing values
df = df.dropna()

locations = df[5].unique()
zipstr = df[1].map(str).str[:5]
zips = zipstr.unique()

with open(locationsOut, 'w') as f:
    for text in locations.tolist():
        f.write(text + '\n')
        
with open(zipsOut, 'w') as f:
    for text in zips.tolist():
        f.write(text + '\n')