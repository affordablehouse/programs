"""
Turn HOA column to boolean
"""

import pandas as pd

# data columns
# 0 PROPERTY TYPE
# 1 ZIP CODE
# 2 PRICE
# 3 BEDS
# 4 BATHS
# 5 LOCATION
# 6 SQUARE FEET
# 7 LOT SIZE
# 8 YEAR BUILT
# 9 HOA/MONTH


fileIn = "../data/la-county/la-county-clean.csv"
fileOut = "../data/la-county/la-county-clean.csv"

# read file to be cleaned
df = pd.read_csv(fileIn, header=None)

# turn empty values into 0
df[9] = df[9].fillna(0)

# convert column to boolean
df[9] = df[9].astype('bool').astype('int')

# output new file
df.to_csv(fileOut, header=None, index=False)
