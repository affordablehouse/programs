"""
Detect anomalies
"""

import pandas as pd

# data columns
# 0 PROPERTY TYPE
# 1 ZIP CODE
# 2 PRICE
# 3 BEDS
# 4 BATHS
# 5 LOCATION
# 6 SQUARE FEET
# 7 LOT SIZE
# 8 YEAR BUILT
# 9 HOA/MONTH

# plan to detect in 2, 3, 4, 6, & 7
# calculate the anomaly above the top of the normal range
# not concerned about the anomalies on the bottom yet
def anomalies(fileIn, columnName, columnNum):
    df = pd.read_csv(fileIn, header=None)
    
    anomaly = df[columnNum].mean() + 2 * df[columnNum].std()
    # fine the anomalies
    column_anomalies = df[df[columnNum] > anomaly]

    # find attributes that aren't anomalies
    df_filtered = df[df[columnNum] < anomaly]
    
    filename = "../data/la-county/anomalies/" + columnName + "-anomalies.csv"
    
    # make csvs
    df_filtered.to_csv("../data/la-county/la-county-clean-no-anomalies.csv", header=None, index=False)
    column_anomalies.to_csv(filename, header=None, index=False)

    # show the anomaly threshold
    print(columnName, anomaly)


anomalies("../data/la-county/la-county-clean.csv", "price", 2)
anomalies("../data/la-county/la-county-clean-no-anomalies.csv", "beds", 3)
anomalies("../data/la-county/la-county-clean-no-anomalies.csv", "baths", 4)
anomalies("../data/la-county/la-county-clean-no-anomalies.csv", "sqft", 6)
anomalies("../data/la-county/la-county-clean-no-anomalies.csv", "lot", 7)
