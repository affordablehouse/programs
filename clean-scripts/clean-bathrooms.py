"""
Remove properties with 0.0 bathrooms.
"""

import pandas as pd

# data columns
# 0 PROPERTY TYPE
# 1 ZIP CODE
# 2 PRICE
# 3 BEDS
# 4 BATHS
# 5 LOCATION
# 6 SQUARE FEET
# 7 LOT SIZE
# 8 YEAR BUILT
# 9 HOA/MONTH

fileIn = "../data/la-county/la-county-clean.csv"
fileOut = "../data/la-county/la-county-clean.csv"

# read file to be cleaned
df = pd.read_csv(fileIn, header=None)

# turn empty bathroom columns into 0
df[4] = df[4].fillna(0)

# filter to only keep records where bathrooms do not equal 0.0
df_filtered = df[df[4] > 0]

# output new file
df_filtered.to_csv(fileOut, header=None, index=False)
